import { useState } from "react";
import axiosInstance from "@utils/config/axios";
import { API_ACCESS } from "@utils/config/endpoint";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { errorState, tokenState, userIdState } from "@store/authState";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useToast } from "react-native-toast-notifications";
import stackState from "@store/stackState";

const useAuth = () => {
  const toast = useToast();

  const stack = useRecoilValue(stackState);
  const setToken = useSetRecoilState(tokenState);
  const setUserId = useSetRecoilState(userIdState);
  const setErr = useSetRecoilState(errorState);
  const setStack = useSetRecoilState(stackState);

  const [isLoading, setIsLoading] = useState(false);

  const login = async (data) => {
    setIsLoading(true);
    setErr(null);

    let body = new FormData();
    body.append("email", data.email);
    body.append("password", data.password);

    await axiosInstance
      .request({
        method: "POST",
        url: stack === "user" ? API_ACCESS.login : API_ACCESS.loginAdmin,
        headers: {
          "Content-Type": "multipart/form-data",
          Accept: "multipart/form-data",
        },
        data: body,
      })
      .then((response) => {
        setToken(response.data.token);
        setUserId(response.data.userId);
        setStack(stack === "user" ? "user" : "admin");
        AsyncStorage.setItem("@token", response.data.token);
        AsyncStorage.setItem("@userId", JSON.stringify(response.data.userId));
        AsyncStorage.setItem("@stack", stack === "user" ? "user" : "admin");
        toast.show(response.data.message);
      })
      .catch((e) => {
        if (typeof e.response.data.message === "string") {
          toast.show(e.response.data.message);
        } else {
          setErr(e.response.data.message);
        }
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const logout = async () => {
    setIsLoading(true);
    setErr(null);
    setToken(null);
    setUserId(null);
    await AsyncStorage.removeItem("@token");
    await AsyncStorage.removeItem("@userId");
    await AsyncStorage.removeItem("@stack");
    toast.show("logout berhasil!");
    setIsLoading(false);
  };

  return {
    isLoading,
    login,
    logout,
  };
};

export default useAuth;
