import { useRecoilState, useRecoilValue } from "recoil";
import { navState } from "@store/navState";
import stackState from "@store/stackState";

const useHandleIntro = () => {
  const nav = useRecoilValue(navState);
  const [stack, setStack] = useRecoilState(stackState);

  const handleLogin = () => {
    if (stack === "user") {
      nav.navigate("UserApp", {
        screen: "Login",
      });
    } else {
      nav.navigate("AdminApp", {
        screen: "Login",
      });
    }
  };

  const handleRegister = () => {
    if (stack === "user") {
      nav.navigate("UserApp", {
        screen: "RegisterUser",
      });
    } else {
      nav.navigate("AdminApp", {
        screen: "RegisterAdmin",
      });
    }
  };

  const handleChangeRole = () => {
    setStack("admin");
    nav.navigate("IntroAdmin");
  };

  return {
    handleLogin,
    handleRegister,
    handleChangeRole,
  };
};

export default useHandleIntro;
