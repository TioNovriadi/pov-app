import axiosInstance from "@utils/config/axios";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { errorState, tokenState } from "@store/authState";
import { useToast } from "react-native-toast-notifications";
import { navState } from "@store/navState";
import { useState } from "react";

const useFetchData = () => {
  const toast = useToast();

  const token = useRecoilValue(tokenState);
  const nav = useRecoilValue(navState);
  const setErrorCheck = useSetRecoilState(errorState);

  const [isLoading, setIsLoading] = useState(false);

  const fetchFunction = async (method, url, body, dest) => {
    if (method === "POST" || method === "PUT") {
      setErrorCheck(null);
    }

    setIsLoading(true);

    await axiosInstance
      .request({
        method,
        url,
        headers: {
          Authorization: token ? `Bearer ${token}` : null,
          Accept: body ? "multipart/form-data" : "application/json",
          "Content-Type": body ? "multipart/form-data" : "application/json",
        },
        data: body ? body : {},
      })
      .then((response) => {
        if (method === "POST" || method === "PUT") {
          toast.show(response.data.message);

          if (dest) {
            nav.navigate(dest);
          }
        } else {
          console.log(response.data);
          return response.data;
        }
      })
      .catch((e) => {
        if (method === "POST" || method === "PUT") {
          if (typeof e.response.data.message === "string") {
            toast.show(e.response.data.message);
          } else {
            setErrorCheck(e.response.data.message);
          }
        }
        console.log(e.response.data);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const fetchDataFunction = async (url) => {
    return await axiosInstance.request({
      method: "GET",
      url,
      headers: {
        Authorization: token ? `Bearer ${token}` : null,
      },
    });
  };

  return {
    isLoading,
    fetchFunction,
    fetchDataFunction,
  };
};

export default useFetchData;
