const BASE_URL = "http://10.0.2.2:3333/";

const AUTH_PREFIX = "auth/";
const USER_PREFIX = "user/";
const CONTENT_PREFIX = "content/";
const PUBLIC_PREFIX = "uploads/";
const ADMIN_PREFIX = "admin/";

export const API_ACCESS = {
  base: BASE_URL,
  login: AUTH_PREFIX + "login",
  loginAdmin: AUTH_PREFIX + "login/admin",
  register: AUTH_PREFIX + "register",
  registerAdmin: AUTH_PREFIX + "register/admin",
  showUserProfile: USER_PREFIX,
  editUserProfile: USER_PREFIX,
  showUserContent: USER_PREFIX + "content",
  showUserBookmark: USER_PREFIX + "bookmark",
  storeContent: CONTENT_PREFIX,
  getAllCheckedContents: CONTENT_PREFIX,
  like: CONTENT_PREFIX + "like",
  unlike: CONTENT_PREFIX + "unlike",
  bookmark: CONTENT_PREFIX + "bookmark",
  unbookmark: CONTENT_PREFIX + "unbookmark",
  comment: CONTENT_PREFIX + "comment",
  userAsset: BASE_URL + PUBLIC_PREFIX + "users",
  contentAsset: BASE_URL + PUBLIC_PREFIX + "content",
  getAllContentAdmin: ADMIN_PREFIX + "content",
  acceptContentAdmin: ADMIN_PREFIX + "accept",
  rejectContentAdmin: ADMIN_PREFIX + "reject",
  deleteContentAdmin: ADMIN_PREFIX + "content",
  updateAndAcceptContentAdmin: ADMIN_PREFIX + "content-accept",
};
