import React from "react";
import MainContainer from "@containers/MainContainer";
import Header from "@components/organisms/Header";
import Form from "@components/organisms/Form";
import LoadingModal from "@components/templates/LoadingModal";
import { errorState } from "@store/authState";
import { useRecoilState } from "recoil";
import useFetchData from "@hooks/useFetchData";
import { API_ACCESS } from "@utils/config/endpoint";

const RegisterAdmin = () => {
  const [errorCek, setErrorCek] = useRecoilState(errorState);

  const { fetchFunction, isLoading } = useFetchData();

  const register = (data) => {
    let body = new FormData();
    body.append("username", data.username);
    body.append("email", data.email);
    body.append("password", data.password);

    fetchFunction("POST", API_ACCESS.registerAdmin, body, "Login");
  };
  return (
    <MainContainer>
      <Header
        withBack
        backDest={"IntroAdmin"}
        backFunc={() => {
          setErrorCek(null);
        }}
        withShadow
      />
      <Form
        withTitle={true}
        title="Buat akun anda"
        submitLabel="Buat Akun"
        extraStyle={{ marginTop: 42 }}
        dataInput={[
          {
            name: "username",
            defaultValue: "",
            type: "text",
            placeholder: "Masukan nama lengkap",
            title: "Nama Lengkap",
            required: true,
            errorCheck: errorCek,
          },
          {
            name: "ajiNum",
            defaultValue: "",
            type: "text",
            placeholder: "Masukan nomor PERS anda",
            title: "Nomor PERS",
            required: true,
            errorCheck: errorCek,
          },
          {
            name: "email",
            defaultValue: "",
            type: "text",
            placeholder: "Masukan email",
            title: "Email",
            required: true,
            errorCheck: errorCek,
          },
          {
            name: "password",
            defaultValue: "",
            type: "password",
            placeholder: "Masukan kata sandi",
            title: "Kata sandi",
            required: true,
            errorCheck: errorCek,
          },
        ]}
        onSubmit={register}
      />

      <LoadingModal visible={isLoading} />
    </MainContainer>
  );
};

export default RegisterAdmin;
