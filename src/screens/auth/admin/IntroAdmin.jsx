import React from "react";
import MainContainer from "@containers/MainContainer";
import Header from "@components/organisms/Header";
import IntroContent from "@components/organisms/IntroContent";

const IntroAdmin = () => {
  return (
    <MainContainer>
      <Header withBack backDest={"IntroUser"} />
      <IntroContent
        desc={"Lihat konten politik yang sudah dikumpulkan. Siapkan diri anda!"}
        withConfirm={false}
      />
    </MainContainer>
  );
};

export default IntroAdmin;
