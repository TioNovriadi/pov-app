import React, { useEffect } from "react";
import MainContainer from "@containers/MainContainer";
import Header from "@components/organisms/Header";
import { useSetRecoilState } from "recoil";
import { navState } from "@store/navState";
import IntroContent from "@components/organisms/IntroContent";
import stackState from "@store/stackState";
import { useIsFocused } from "@react-navigation/native";

const IntroUser = ({ navigation }) => {
  const setNav = useSetRecoilState(navState);
  const setStack = useSetRecoilState(stackState);

  const isFocused = useIsFocused();

  useEffect(() => {
    setNav(navigation);

    if (isFocused) {
      setStack("user");
    }
  }, [isFocused]);

  return (
    <MainContainer>
      <Header />
      <IntroContent desc={"Lihat politik apa yang sedang terjadi saat ini!"} />
    </MainContainer>
  );
};

export default IntroUser;
