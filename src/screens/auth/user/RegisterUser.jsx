import React from "react";
import MainContainer from "@containers/MainContainer";
import Header from "@components/organisms/Header";
import Form from "@components/organisms/Form";
import { useRecoilState } from "recoil";
import { errorState } from "@store/authState";
import useFetchData from "@hooks/useFetchData";
import { API_ACCESS } from "@utils/config/endpoint";
import LoadingModal from "@components/templates/LoadingModal";

const RegisterUser = () => {
  const { fetchFunction, isLoading } = useFetchData();

  const [errorCek, setErrorCek] = useRecoilState(errorState);

  const register = (data) => {
    let body = new FormData();
    body.append("username", data.username);
    body.append("email", data.email);
    body.append("password", data.password);

    fetchFunction("POST", API_ACCESS.register, body, "Login");
  };

  return (
    <MainContainer>
      <Header
        withBack
        backDest={"IntroUser"}
        backFunc={() => {
          setErrorCek(null);
        }}
        withShadow
      />
      <Form
        withTitle={true}
        title="Buat akun anda"
        submitLabel="Buat Akun"
        extraStyle={{ marginTop: 42 }}
        dataInput={[
          {
            name: "username",
            defaultValue: "",
            type: "text",
            placeholder: "Masukan nama pengguna",
            title: "Nama Pengguna",
            required: true,
            errorCheck: errorCek,
          },
          {
            name: "email",
            defaultValue: "",
            type: "text",
            placeholder: "Masukan email",
            title: "Email",
            required: true,
            errorCheck: errorCek,
          },
          {
            name: "password",
            defaultValue: "",
            type: "password",
            placeholder: "Masukan kata sandi",
            title: "Kata sandi",
            required: true,
            errorCheck: errorCek,
          },
        ]}
        onSubmit={register}
      />

      <LoadingModal visible={isLoading} />
    </MainContainer>
  );
};

export default RegisterUser;
