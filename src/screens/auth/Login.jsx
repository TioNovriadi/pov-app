import React from "react";
import MainContainer from "@containers/MainContainer";
import Header from "@components/organisms/Header";
import { useRecoilState, useRecoilValue } from "recoil";
import stackState from "@store/stackState";
import Form from "@components/organisms/Form";
import useAuth from "@hooks/useAuth";
import { errorState } from "@store/authState";
import LoadingModal from "@components/templates/LoadingModal";

const Login = () => {
  const stack = useRecoilValue(stackState);
  const [errorCek, setErrorCek] = useRecoilState(errorState);

  const { login, isLoading } = useAuth();

  return (
    <MainContainer>
      <Header
        withBack
        backDest={stack === "user" ? "IntroUser" : "IntroAdmin"}
        backFunc={() => setErrorCek(null)}
        withShadow
      />
      <Form
        withTitle={true}
        title={"Masuk ke akun anda"}
        extraStyle={{ marginTop: 42 }}
        onSubmit={login}
        dataInput={[
          {
            name: "email",
            defaultValue: "",
            type: "text",
            placeholder: "Masukan email",
            title: "Email",
            required: true,
            errorCheck: errorCek,
          },
          {
            name: "password",
            defaultValue: "",
            type: "password",
            placeholder: "Masukan kata sandi",
            title: "Kata sandi",
            required: true,
            errorCheck: errorCek,
          },
        ]}
      />

      <LoadingModal visible={isLoading} />
    </MainContainer>
  );
};

export default Login;
