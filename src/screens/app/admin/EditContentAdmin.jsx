import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import React, { useState } from "react";
import MainContainer from "@containers/MainContainer";
import Header from "@components/organisms/Header";
import { useRecoilState } from "recoil";
import { errorState } from "@store/authState";
import Form from "@components/organisms/Form";
import { useForm } from "react-hook-form";
import colors from "@themes/colors";
import fonts from "@themes/fonts";
import useFetchData from "@hooks/useFetchData";
import AdminConfirmModal from "@components/templates/AdminConfirmModal";
import { refreshState } from "@store/refreshState";
import { API_ACCESS } from "@utils/config/endpoint";

const EditContentAdmin = ({ route }) => {
  const { data } = route.params;

  const [errorCheck, setErrorCheck] = useRecoilState(errorState);
  const [refresh, setRefresh] = useRecoilState(refreshState);

  const [showConfirmModal, setShowConfirmModal] = useState(false);

  const { control, handleSubmit } = useForm();
  const { fetchFunction, isLoading } = useFetchData();

  const handlePublish = (dataInput) => {
    let body = new FormData();
    body.append("title", dataInput.title);
    body.append("story", dataInput.story);

    fetchFunction(
      "PUT",
      API_ACCESS.updateAndAcceptContentAdmin + `/${data.id}`,
      body,
      "HomeAdmin"
    );
    setRefresh(!refresh);
  };

  return (
    <MainContainer>
      <Header
        withShadow
        withBack
        backDest={"HomeAdmin"}
        backFunc={() => setErrorCheck(null)}
      />
      <View style={styles.header}>
        <View
          style={[
            styles.statusContainer,
            { backgroundColor: data.anonymous ? colors.Red : colors.Black },
          ]}
        >
          <Text style={styles.status}>
            {data.anonymous ? "Anon" : data.author.profile.username}
          </Text>
        </View>
        <TouchableOpacity onPress={() => setShowConfirmModal(true)}>
          <Text style={styles.label}>Kirim</Text>
        </TouchableOpacity>
      </View>
      <Form
        extraStyle={{
          marginTop: 30,
        }}
        dataInput={[
          {
            name: "image",
            defaultValue: data.image,
            placeholder: require("@assets/images/imagePlaceholder.png"),
            type: "image",
            disable: true,
          },
          {
            name: "title",
            defaultValue: data.title,
            placeholder: "Tulis judul disini!",
            type: "text",
            errorCheck: errorCheck,
          },
          {
            name: "story",
            defaultValue: data.story,
            placeholder: "Tulis artikel disini!",
            type: "textarea",
            errorCheck: errorCheck,
          },
        ]}
        withSubmit={false}
        controlPass={control}
        app
      />

      <AdminConfirmModal
        visibile={showConfirmModal}
        question="Apakah anda ingin menerbitkan berita ini?"
        confirmLabel="Terbitkan"
        onReject={() => setShowConfirmModal(false)}
        onConfirm={handleSubmit(handlePublish)}
        isLoading={isLoading}
        final="Terima kasih! Berita ini sudah diterbitkan"
      />
    </MainContainer>
  );
};

export default EditContentAdmin;

const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginHorizontal: 45,
    marginTop: 13,
  },
  statusContainer: {
    paddingHorizontal: 48,
    paddingVertical: 6,
    borderRadius: 38,
  },
  status: {
    fontFamily: fonts.Medium,
    fontSize: 16,
    color: colors.White,
  },
  label: {
    fontFamily: fonts.Medium,
    fontSize: 13,
    color: colors.Black,
  },
});
