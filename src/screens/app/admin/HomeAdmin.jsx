import React, { useEffect } from "react";
import MainContainer from "@containers/MainContainer";
import Header from "@components/organisms/Header";
import { useQuery } from "react-query";
import useFetchData from "@hooks/useFetchData";
import { API_ACCESS } from "@utils/config/endpoint";
import LoadingScreen from "@components/templates/LoadingScreen";
import AdminContentList from "@components/organisms/AdminContentList";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { refreshState } from "@store/refreshState";
import { navState } from "@store/navState";

const HomeAdmin = ({ navigation }) => {
  const refresh = useRecoilValue(refreshState);
  const setNav = useSetRecoilState(navState);

  const { fetchDataFunction } = useFetchData();

  const { isLoading, data, refetch } = useQuery(["getAllContents"], () =>
    fetchDataFunction(API_ACCESS.getAllContentAdmin, "GET")
  );

  useEffect(() => {
    setNav(navigation);
  }, []);

  useEffect(() => {
    refetch();
  }, [refresh]);

  return (
    <MainContainer>
      <Header withShadow withLogout />
      {isLoading ? (
        <LoadingScreen />
      ) : (
        <AdminContentList data={data.data.data} />
      )}
    </MainContainer>
  );
};

export default HomeAdmin;
