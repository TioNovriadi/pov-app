import React, { useEffect } from "react";
import MainContainer from "@containers/MainContainer";
import Header from "@components/organisms/Header";
import POVAddButton from "@components/atoms/POVAddButton";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { navState } from "@store/navState";
import { useQuery } from "react-query";
import useFetchData from "@hooks/useFetchData";
import { API_ACCESS } from "@utils/config/endpoint";
import LoadingScreen from "@components/templates/LoadingScreen";
import HomeContentList from "@components/organisms/HomeContentList";
import { refreshState } from "@store/refreshState";

const Home = ({ navigation }) => {
  const setNav = useSetRecoilState(navState);
  const refresh = useRecoilValue(refreshState);

  const { fetchDataFunction } = useFetchData();

  const { isLoading, data, refetch } = useQuery(["getAllCheckedContents"], () =>
    fetchDataFunction(API_ACCESS.getAllCheckedContents)
  );

  useEffect(() => {
    setNav(navigation);
  }, []);

  useEffect(() => {
    refetch();
  }, [refresh]);

  return (
    <MainContainer>
      <Header withShadow />
      {isLoading ? (
        <LoadingScreen />
      ) : (
        <HomeContentList data={data.data.data} />
      )}
      <POVAddButton />
    </MainContainer>
  );
};

export default Home;
