import React from "react";
import MainContainer from "@containers/MainContainer";
import { useQuery } from "react-query";
import useFetchData from "@hooks/useFetchData";
import { API_ACCESS } from "@utils/config/endpoint";
import LoadingScreen from "@components/templates/LoadingScreen";
import Header from "@components/organisms/Header";
import ContentList from "@components/organisms/ContentList";

const Kontenku = () => {
  const { fetchDataFunction } = useFetchData();
  const { isLoading, data } = useQuery(["showUserContent"], () =>
    fetchDataFunction(API_ACCESS.showUserContent)
  );

  if (isLoading) return <LoadingScreen />;

  console.log(data.data.data[0]);

  return (
    <MainContainer>
      <Header withBack backDest="Account" withShadow />
      <ContentList data={data.data.data[0]} />
    </MainContainer>
  );
};

export default Kontenku;
