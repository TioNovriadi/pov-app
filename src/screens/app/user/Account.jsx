import React from "react";
import MainContainer from "@containers/MainContainer";
import Header from "@components/organisms/Header";
import AccountContent from "@components/organisms/AccountContent";

const Account = () => {
  return (
    <MainContainer>
      <Header withShadow />
      <AccountContent />
    </MainContainer>
  );
};

export default Account;
