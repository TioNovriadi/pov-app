import { View, Text } from "react-native";
import React from "react";
import useFetchData from "@hooks/useFetchData";
import { API_ACCESS } from "@utils/config/endpoint";
import LoadingScreen from "@components/templates/LoadingScreen";
import MainContainer from "@containers/MainContainer";
import Form from "@components/organisms/Form";
import { useQuery } from "react-query";
import Header from "@components/organisms/Header";

const EditAccount = () => {
  const { fetchDataFunction, fetchFunction } = useFetchData();

  const { isLoading, data } = useQuery(["showUserProfile"], () =>
    fetchDataFunction(API_ACCESS.showUserProfile, "GET")
  );

  if (isLoading) return <LoadingScreen />;

  const handleEdit = (dataIn) => {
    const body = new FormData();
    if (dataIn.profilePic !== data.data.data[0].profile.profile_pic) {
      body.append("profilePic", dataIn.profilePic);
    }
    body.append("username", dataIn.username);
    body.append("phone", dataIn.phone);

    if (dataIn.email !== data.data.data[0].email) {
      body.append("email", dataIn.email);
    }

    fetchFunction("PUT", API_ACCESS.editUserProfile, body, "Account");
  };

  return (
    <MainContainer>
      <Header withBack backDest="Account" withShadow />
      <Form
        extraStyle={{ marginTop: 30 }}
        dataInput={[
          {
            type: "profile",
            name: "profilePic",
            defaultValue: data.data.data[0].profile.profile_pic,
          },
          {
            type: "text",
            name: "username",
            defaultValue: data.data.data[0].profile.username,
            placeholder: "Masukan nama pengguna",
            title: "Nama Pengguna",
          },
          {
            type: "text",
            name: "phone",
            defaultValue: data.data.data[0].profile.phone,
            placeholder: "Masukan nomor telepon anda",
            title: "Telepon",
          },
          {
            type: "text",
            name: "email",
            defaultValue: data.data.data[0].email,
            placeholder: "Masukan email anda",
            title: "Email",
          },
        ]}
        onSubmit={handleEdit}
        submitLabel="Simpan"
      />
    </MainContainer>
  );
};

export default EditAccount;
