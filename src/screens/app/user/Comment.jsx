import { StyleSheet } from "react-native";
import React from "react";
import Header from "@components/organisms/Header";
import ContentBox from "@components/molecules/ContentBox";
import CommentSection from "@components/organisms/CommentSection";
import ScrollContainer from "@containers/ScrollContainer";

const Comment = ({ route }) => {
  const { data, editorUsername, bookmark } = route.params;

  return (
    <ScrollContainer>
      <Header withBack backDest="Home" withShadow />
      <ContentBox
        item={data}
        user={data.author.profile}
        home
        bookmark={bookmark}
        editor={editorUsername}
      />
      <CommentSection contentId={data.id} dataComment={data.comments} />
    </ScrollContainer>
  );
};

export default Comment;

const styles = StyleSheet.create({});
