import { View, Text } from "react-native";
import React from "react";
import MainContainer from "@containers/MainContainer";
import { useQuery } from "react-query";
import useFetchData from "@hooks/useFetchData";
import { API_ACCESS } from "@utils/config/endpoint";
import Header from "@components/organisms/Header";
import LoadingScreen from "@components/templates/LoadingScreen";
import HomeContentList from "@components/organisms/HomeContentList";

const Bookmark = () => {
  const { fetchDataFunction } = useFetchData();
  const { isLoading, data } = useQuery(["showUserBookmark"], () =>
    fetchDataFunction(API_ACCESS.showUserBookmark)
  );

  return (
    <MainContainer>
      <Header withBack backDest="Account" withShadow />
      {isLoading ? (
        <LoadingScreen />
      ) : (
        <HomeContentList data={data.data.data[0].bookmarks} bookmark />
      )}
    </MainContainer>
  );
};

export default Bookmark;
