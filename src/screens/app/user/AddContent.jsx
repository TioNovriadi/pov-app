import React, { useState } from "react";
import MainContainer from "@containers/MainContainer";
import Header from "@components/organisms/Header";
import AddContentFormHead from "@components/molecules/AddContentFormHead";
import { useQuery } from "react-query";
import useFetchData from "@hooks/useFetchData";
import { API_ACCESS } from "@utils/config/endpoint";
import LoadingScreen from "@components/templates/LoadingScreen";
import { useForm } from "react-hook-form";
import Form from "@components/organisms/Form";
import { useRecoilState, useRecoilValue } from "recoil";
import { errorState } from "@store/authState";
import ConfirmModal from "@components/templates/ConfirmModal";
import LoadingModal from "@components/templates/LoadingModal";
import { navState } from "@store/navState";

const AddContent = () => {
  const fetch = useFetchData();

  const { isLoading, data } = useQuery(["showUserProfile"], () =>
    fetch.fetchDataFunction(API_ACCESS.showUserProfile, "GET")
  );

  const { control, handleSubmit } = useForm();

  const [errorCek, setErrorCek] = useRecoilState(errorState);
  const nav = useRecoilValue(navState);

  const [send, setSend] = useState(false);
  const [validate, setValidate] = useState(false);

  const addContent = (data) => {
    setSend(false);
    let body = new FormData();
    body.append("anonymous", data.anonymous);
    body.append("title", data.title);
    body.append("story", data.story);

    if (data.image !== null) {
      body.append("image", data.image);
    }

    fetch.fetchFunction("POST", API_ACCESS.storeContent, body);

    setValidate(true);
  };

  const onConfirm = () => {
    setValidate(false);
    nav.navigate("Home");
  };

  if (isLoading) return <LoadingScreen />;

  return (
    <MainContainer>
      <Header
        withShadow
        withBack
        backDest={"Home"}
        backFunc={() => setErrorCek(null)}
      />
      <AddContentFormHead
        data={data.data.data}
        control={control}
        onSubmit={() => setSend(true)}
      />
      <Form
        extraStyle={{
          marginTop: 30,
        }}
        dataInput={[
          {
            name: "image",
            defaultValue: null,
            placeholder: require("@assets/images/imagePlaceholder.png"),
            type: "image",
          },
          {
            name: "title",
            defaultValue: "",
            placeholder: "Tulis judul disini!",
            type: "text",
            errorCheck: errorCek,
          },
          {
            name: "story",
            defaultValue: "",
            placeholder: "Tulis artikel disini!",
            type: "textarea",
            errorCheck: errorCek,
          },
        ]}
        withSubmit={false}
        controlPass={control}
        app
      />

      <ConfirmModal
        visible={send}
        question="Apakah anda akan mengirim berita ini?"
        confirmLabel="Kirim"
        rejectLabel="Batalkan"
        onConfirm={handleSubmit(addContent)}
        onReject={() => setSend(false)}
      />

      <ConfirmModal
        visible={validate}
        title="Pemberitahuan"
        titleIcon={require("@assets/images/info.png")}
        question="Berita anda sudah di kirim. Berita akan segera ditinjau kurang lebih 24 jam. Terima kasih!"
        confirmLabel="Oke"
        onConfirm={onConfirm}
      />

      <LoadingModal visible={fetch.isLoading} />
    </MainContainer>
  );
};

export default AddContent;
