import React, { useCallback, useEffect, useState } from "react";
import { NavigationContainer } from "@react-navigation/native";
import AuthStack from "@routes/AuthStack";
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil";
import { isLoggedInSelector, tokenState, userIdState } from "@store/authState";
import AsyncStorage from "@react-native-async-storage/async-storage";
import * as SplashScreen from "expo-splash-screen";
import { StyleSheet, View } from "react-native";
import stackState from "@store/stackState";
import UserStack from "@routes/UserStack";
import AdminStack from "@routes/AdminStack";

const AppNav = ({ fontsLoaded }) => {
  const [token, setToken] = useRecoilState(tokenState);
  const setUserId = useSetRecoilState(userIdState);
  const isLoggedIn = useRecoilValue(isLoggedInSelector);
  const [stack, setStack] = useRecoilState(stackState);

  const [appReady, setAppReady] = useState(false);

  const onLayoutRootView = useCallback(async () => {
    if (fontsLoaded && appReady === true) {
      await SplashScreen.hideAsync();
    }
  }, [fontsLoaded, appReady]);

  useEffect(() => {
    const checkLogin = async () => {
      const tokenCheck = await AsyncStorage.getItem("@token");
      const userIdCheck = await AsyncStorage.getItem("@userId");
      const stackCheck = await AsyncStorage.getItem("@stack");

      if (tokenCheck) {
        setToken(tokenCheck);
        setUserId(JSON.parse(userIdCheck));
        setStack(stackCheck);
      }
    };

    checkLogin();
    setAppReady(true);
  }, [token]);

  if (!fontsLoaded || appReady === false) {
    return null;
  }

  return (
    <View onLayout={onLayoutRootView} style={styles.container}>
      <NavigationContainer>
        {!isLoggedIn ? (
          <AuthStack />
        ) : stack === "user" ? (
          <UserStack />
        ) : (
          <AdminStack />
        )}
      </NavigationContainer>
    </View>
  );
};

export default AppNav;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
