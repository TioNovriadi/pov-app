const { atom } = require("recoil");

const stackState = atom({
  key: "stackState",
  default: "user",
});

export default stackState;
