const { atom } = require("recoil");

const refreshState = atom({
  key: "refreshState",
  default: false,
});

export { refreshState };
