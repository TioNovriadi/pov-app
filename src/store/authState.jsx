const { atom, selector } = require("recoil");

const tokenState = atom({
  key: "tokenState",
  default: null,
});

const userIdState = atom({
  key: "userIdState",
  default: null,
});

const errorState = atom({
  key: "errorState",
  default: null,
});

const isLoggedInSelector = selector({
  key: "isLoggedInSelector",
  get: ({ get }) => {
    const userToken = get(tokenState);
    return !!userToken;
  },
});

export { tokenState, errorState, isLoggedInSelector, userIdState };
