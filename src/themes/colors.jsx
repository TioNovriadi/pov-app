const colors = {
  White: "#FFFFFF",
  Black: "#000000",
  Red: "#BC0000",
  LightRed: "#FF000F",
  BlurRed: "rgba(189, 1, 1, 0.25)",
  LightGrey: "#F5F5F5",
  Grey: "#5D5D5D",
  DarkGrey: "#8A8A8A",
  BlurBlack: "rgba(0, 0, 0, 0.25)",
  Placeholder: "#8F8F8F",
  Modal: "rgba(0, 0, 0, 0.25)",
  Success: "#327200",
  Off: "#4C4C4C",
};

export default colors;
