const fonts = {
  Regular: "Made-Tommy-Regular",
  Medium: "Made-Tommy-Medium",
  Bold: "Made-Tommy-Bold",
};

export default fonts;
