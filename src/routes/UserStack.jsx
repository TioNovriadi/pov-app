import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Home from "@screens/app/user/Home";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import colors from "@themes/colors";
import { Image } from "react-native";
import AddContent from "@screens/app/user/AddContent";
import Account from "@screens/app/user/Account";
import EditAccount from "@screens/app/user/EditAccount";
import Kontenku from "@screens/app/user/Kontenku";
import Bookmark from "@screens/app/user/Bookmark";
import Comment from "@screens/app/user/Comment";

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const UserStack = () => {
  return (
    <Stack.Navigator initialRouteName="MainApp">
      <Stack.Screen
        name="MainApp"
        component={MainApp}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

const MainApp = () => {
  return (
    <Tab.Navigator
      initialRouteName="HomeStack"
      screenOptions={({ route }) => ({
        tabBarStyle: {
          backgroundColor: colors.Red,
          height: 70,
        },
        tabBarShowLabel: false,
        tabBarIcon: ({ focused }) => {
          if (route.name === "HomeStack") {
            return (
              <Image
                source={
                  focused
                    ? require("@assets/images/homeActive.png")
                    : require("@assets/images/homeInactive.png")
                }
              />
            );
          } else if (route.name === "AccountStack") {
            return (
              <Image
                source={
                  focused
                    ? require("@assets/images/accountActive.png")
                    : require("@assets/images/accountInactive.png")
                }
              />
            );
          }
        },
        unmountOnBlur: true,
      })}
    >
      <Tab.Screen
        name="HomeStack"
        component={HomeStack}
        options={{ headerShown: false }}
      />
      <Tab.Screen
        name="AccountStack"
        component={AccountStack}
        options={{ headerShown: false }}
      />
    </Tab.Navigator>
  );
};

const HomeStack = () => {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen
        name="Home"
        component={Home}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="AddContent"
        component={AddContent}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Comment"
        component={Comment}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

const AccountStack = () => {
  return (
    <Stack.Navigator initialRouteName="Account">
      <Stack.Screen
        name="Account"
        component={Account}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="EditAccount"
        component={EditAccount}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Kontenku"
        component={Kontenku}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Bookmark"
        component={Bookmark}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

export default UserStack;
