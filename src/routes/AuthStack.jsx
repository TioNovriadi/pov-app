import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import IntroUser from "@screens/auth/user/IntroUser";
import IntroAdmin from "@screens/auth/admin/IntroAdmin";
import Login from "@screens/auth/Login";
import RegisterAdmin from "@screens/auth/admin/RegisterAdmin";
import RegisterUser from "@screens/auth/user/RegisterUser";

const Stack = createNativeStackNavigator();

const AuthStack = () => {
  return (
    <Stack.Navigator initialRouteName="IntroUser">
      <Stack.Screen
        name="IntroUser"
        component={IntroUser}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="IntroAdmin"
        component={IntroAdmin}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="AdminApp"
        component={AdminApp}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="UserApp"
        component={UserApp}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

const AdminApp = () => {
  return (
    <Stack.Navigator initialRouteName="Login">
      <Stack.Screen
        name="Login"
        component={Login}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="RegisterAdmin"
        component={RegisterAdmin}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

const UserApp = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Login"
        component={Login}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="RegisterUser"
        component={RegisterUser}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

export default AuthStack;
