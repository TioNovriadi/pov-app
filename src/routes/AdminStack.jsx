import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import HomeAdmin from "@screens/app/admin/HomeAdmin";
import EditContentAdmin from "@screens/app/admin/EditContentAdmin";

const Stack = createNativeStackNavigator();

const AdminStack = () => {
  return (
    <Stack.Navigator initialRouteName="HomeAdmin">
      <Stack.Screen
        name="HomeAdmin"
        component={HomeAdmin}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="EditContentAdmin"
        component={EditContentAdmin}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

export default AdminStack;
