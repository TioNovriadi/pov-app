import { ScrollView, StyleSheet } from "react-native";
import React from "react";
import colors from "@themes/colors";

const ScrollContainer = ({ children }) => {
  return (
    <ScrollView style={styles.container} contentContainerStyle={styles.content}>
      {children}
    </ScrollView>
  );
};

export default ScrollContainer;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.White,
  },
  content: {
    paddingBottom: 100,
  },
});
