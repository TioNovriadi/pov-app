import { StyleSheet, View } from "react-native";
import React from "react";
import colors from "@themes/colors";

/**
 * Creates a container component for a React Native application.
 * The container is customizable and reusable across different screens.
 *
 * @returns {React.Component} The container component.
 */
const MainContainer = ({ children }) => {
  return <View style={styles.container}>{children}</View>;
};

export default MainContainer;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.White,
  },
});
