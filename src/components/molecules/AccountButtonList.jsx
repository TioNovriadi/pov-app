import { FlatList, StyleSheet } from "react-native";
import React from "react";
import POVAccountButton from "@components/atoms/POVAccountButton";
import { useRecoilValue } from "recoil";
import { navState } from "@store/navState";
import useAuth from "@hooks/useAuth";

const AccountButtonList = () => {
  const { logout } = useAuth();

  const nav = useRecoilValue(navState);

  const dataBtn = [
    {
      icon: require("@assets/images/akun.png"),
      label: "Akun",
      onPress: () => nav.navigate("EditAccount"),
    },
    {
      icon: require("@assets/images/kontenKu.png"),
      label: "Kontenku",
      onPress: () => nav.navigate("Kontenku"),
    },
    {
      icon: require("@assets/images/tandai.png"),
      label: "Tandai",
      onPress: () => nav.navigate("Bookmark"),
    },
    {
      icon: require("@assets/images/logout.png"),
      label: "Keluar",
      onPress: logout,
    },
  ];

  return (
    <FlatList
      data={dataBtn}
      contentContainerStyle={styles.list}
      renderItem={({ item }) => (
        <POVAccountButton
          icon={item.icon}
          label={item.label}
          onPress={item.onPress}
        />
      )}
    />
  );
};

export default AccountButtonList;

const styles = StyleSheet.create({
  list: {
    gap: 40,
  },
});
