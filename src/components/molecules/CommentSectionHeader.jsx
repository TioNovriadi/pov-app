import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import React, { useState } from "react";
import fonts from "@themes/fonts";
import colors from "@themes/colors";
import CommentModal from "@components/templates/CommentModal";
import useFetchData from "@hooks/useFetchData";
import { useForm } from "react-hook-form";
import { API_ACCESS } from "@utils/config/endpoint";
import { useRecoilState } from "recoil";
import { refreshState } from "@store/refreshState";

const CommentSectionHeader = ({ contentId }) => {
  const [refresh, setRefresh] = useRecoilState(refreshState);

  const [showCommentModal, setShowCommentModal] = useState(false);

  const { fetchFunction, isLoading } = useFetchData();
  const { control, handleSubmit } = useForm();

  const handleReject = () => {
    setShowCommentModal(false);
  };

  const handleConfirm = (data) => {
    let body = new FormData();
    body.append("comment", data.message);

    fetchFunction("POST", API_ACCESS.comment + `/${contentId}`, body);
    setShowCommentModal(false);
    setRefresh(!refresh);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Komentar</Text>
      <TouchableOpacity onPress={() => setShowCommentModal(true)}>
        <Text style={styles.label}>Tulis Komentar</Text>
      </TouchableOpacity>

      <CommentModal
        visible={showCommentModal}
        isLoading={isLoading}
        control={control}
        onReject={handleReject}
        onConfirm={handleSubmit(handleConfirm)}
      />
    </View>
  );
};

export default CommentSectionHeader;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  title: {
    fontFamily: fonts.Medium,
    fontSize: 23,
    color: colors.Black,
  },
  label: {
    fontFamily: fonts.Medium,
    fontSize: 12,
    color: colors.Grey,
  },
});
