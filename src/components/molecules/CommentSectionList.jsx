import { StyleSheet, View } from "react-native";
import React from "react";
import CommentBox from "@components/atoms/CommentBox";

const CommentSectionList = ({ data }) => {
  return (
    <View style={styles.container}>
      {data.map((item, index) => (
        <CommentBox key={index.toString()} item={item} />
      ))}
    </View>
  );
};

export default CommentSectionList;

const styles = StyleSheet.create({
  container: {
    gap: 12,
  },
});
