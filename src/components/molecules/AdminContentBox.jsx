import { StyleSheet, Text, View } from "react-native";
import React from "react";
import POVContentProfile from "@components/atoms/POVContentProfile";

const AdminContentBox = ({ item }) => {
  return (
    <View>
      <POVContentProfile />
    </View>
  );
};

export default AdminContentBox;

const styles = StyleSheet.create({});
