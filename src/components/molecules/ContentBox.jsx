import { StyleSheet, View } from "react-native";
import React from "react";
import POVContentProfile from "@components/atoms/POVContentProfile";
import { API_ACCESS } from "@utils/config/endpoint";
import POVContentFill from "@components/atoms/POVContentFill";

const ContentBox = ({ item, user, editor, bookmark, admin, home }) => {
  return (
    <View style={styles.container}>
      <POVContentProfile
        image={
          user.profile_pic
            ? {
                uri: `${API_ACCESS.userAsset}/${user.profile_pic}`,
              }
            : require("@assets/images/user.png")
        }
      />
      <POVContentFill
        item={item}
        username={user.username}
        admin={admin}
        home={home}
        editorUsername={editor}
        bookmark={bookmark}
      />
    </View>
  );
};

export default ContentBox;

const styles = StyleSheet.create({
  container: {
    paddingLeft: 20,
    paddingRight: 35,
    paddingVertical: 30,
    flexDirection: "row",
    gap: 13.48,
  },
});
