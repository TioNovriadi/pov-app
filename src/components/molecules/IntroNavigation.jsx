import { StyleSheet, View } from "react-native";
import React from "react";
import POVButton from "@components/atoms/POVButton";
import POVTextButton from "@components/atoms/POVTextButton";
import PropTypes from "prop-types";
import useHandleIntro from "@hooks/useHandleIntro";

/**
 * Renders a view containing buttons for login and registration, and an optional button to change the user's role.
 *
 * @param {Object} props - The component props.
 * @param {boolean} props.withConfirm - Indicates whether or not to include the button to change the user's role.
 * @returns {JSX.Element} The rendered component.
 */
const IntroNavigation = ({ withConfirm }) => {
  const { handleLogin, handleRegister, handleChangeRole } = useHandleIntro();

  return (
    <View style={styles.container}>
      <POVButton label={"Masuk"} onPress={handleLogin} />
      <POVButton label={"Daftar"} onPress={handleRegister} />
      {withConfirm === true && (
        <POVTextButton
          label={"Anda Jurnalis Profesional?"}
          onPress={handleChangeRole}
        />
      )}
    </View>
  );
};

IntroNavigation.defaultProps = {
  withConfirm: true,
};

IntroNavigation.propTypes = {
  withConfirm: PropTypes.bool,
};

export default IntroNavigation;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 42,
    marginTop: 138,
    gap: 24,
  },
});
