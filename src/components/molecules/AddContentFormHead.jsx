import { StyleSheet, View } from "react-native";
import React from "react";
import POVButton from "@components/atoms/POVButton";
import POVTextButton from "@components/atoms/POVTextButton";
import colors from "@themes/colors";
import fonts from "@themes/fonts";
import { useController } from "react-hook-form";
import PropTypes from "prop-types";

/**
 * Renders a view containing two buttons, one for toggling anonymous mode and the other for submitting the form.
 *
 * @param {Array} data - An array of data used to populate the component.
 * @param {Function} onSubmit - A function to be executed when the submit button is pressed.
 * @returns {JSX.Element} - A view containing two buttons.
 */
const AddContentFormHead = ({ data, control, onSubmit }) => {
  const { field } = useController({
    name: "anonymous",
    defaultValue: true,
    control,
  });

  return (
    <View style={styles.container}>
      <POVButton
        label={field.value === true ? "Anonim" : data[0].profile.username}
        size="medium"
        onPress={() => field.onChange(!field.value)}
        color={field.value === true ? colors.Red : colors.Black}
        withShadow={true}
      />
      <POVTextButton
        label="Kirim"
        color={colors.Black}
        extraStyle={styles.textBtn}
        withShadow
        onPress={onSubmit}
      />
    </View>
  );
};

AddContentFormHead.propTypes = {
  data: PropTypes.array,
  onSubmit: PropTypes.func,
};

export default AddContentFormHead;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginHorizontal: 45,
    marginTop: 13,
  },
  textBtn: {
    fontSize: 13,
    fontFamily: fonts.Medium,
  },
});
