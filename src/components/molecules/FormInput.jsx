import { FlatList, StyleSheet, View } from "react-native";
import React from "react";
import PropTypes from "prop-types";
import POVRoundTextInput from "@components/atoms/POVRoundTextInput";
import POVImagePicker from "@components/atoms/POVImagePicker";
import POVTextArea from "@components/atoms/POVTextArea";
import POVProfilePicker from "@components/atoms/POVProfilePicker";

/**
 * Render a form input flatlist base an the data input type
 *
 * @param {Array} dataInput - An array of data used for the flatlist data consist of input type
 * @param {object} control - The control object used by the 'react-hook-form' library to control the input field.
 * @param {bool} app - A boolean tu confirm if the form was in app or in auth
 * @returns {JSX.Element} - The rendered flatlist with input based on type
 */

const FormInput = ({ dataInput, control, app }) => {
  return (
    <View>
      <FlatList
        data={dataInput}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={[
          styles.list,
          { paddingBottom: app === true ? 200 : 0 },
        ]}
        renderItem={({ item }) => {
          if (item.type === "text") {
            return (
              <POVRoundTextInput
                title={item.title ? item.title : null}
                name={item.name}
                defaultValue={item.defaultValue}
                control={control}
                placeholder={item.placeholder}
                required={item.required ? item.required : false}
                errorCheck={item.errorCheck ? item.errorCheck : null}
              />
            );
          } else if (item.type === "password") {
            return (
              <POVRoundTextInput
                title={item.title ? item.title : null}
                name={item.name}
                defaultValue={item.defaultValue}
                control={control}
                placeholder={item.placeholder}
                isPassword={true}
                required={item.required ? item.required : false}
                errorCheck={item.errorCheck ? item.errorCheck : null}
              />
            );
          } else if (item.type === "image") {
            return (
              <POVImagePicker
                name={item.name}
                defaultValue={item.defaultValue}
                control={control}
                placeholder={item.placeholder}
                disable={item.disable ? item.disable : false}
              />
            );
          } else if (item.type === "textarea") {
            return (
              <POVTextArea
                name={item.name}
                defaultValue={item.defaultValue}
                control={control}
                placeholder={item.placeholder}
                errorCheck={item.errorCheck ? item.errorCheck : null}
              />
            );
          } else if (item.type === "profile") {
            return (
              <POVProfilePicker
                name={item.name}
                defaultValue={item.defaultValue}
                control={control}
              />
            );
          }
        }}
      />
    </View>
  );
};

FormInput.defaultProps = {
  app: false,
};

FormInput.propTypes = {
  dataInput: PropTypes.arrayOf(PropTypes.object).isRequired,
  control: PropTypes.object.isRequired,
  app: PropTypes.bool,
};

export default FormInput;

const styles = StyleSheet.create({
  list: {
    gap: 21,
  },
});
