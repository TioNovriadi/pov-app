import {
  ActivityIndicator,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import React, { useState } from "react";
import { BlurView } from "expo-blur";
import colors from "@themes/colors";
import fonts from "@themes/fonts";
import AdminMessage from "@components/organisms/AdminMessage";

const AdminConfirmModal = ({
  visibile,
  question,
  confirmLabel,
  onConfirm,
  onReject,
  withMessage,
  isLoading,
  control,
  final,
}) => {
  const [changeMessage, setChangeMessage] = useState(false);
  const [done, setDone] = useState(false);

  return (
    <Modal transparent visible={visibile}>
      <BlurView
        style={[styles.container, { alignItems: isLoading ? "center" : null }]}
        intensity={1}
        tint="light"
      >
        {isLoading ? (
          <View style={styles.loader}>
            <ActivityIndicator size="large" />
          </View>
        ) : changeMessage ? (
          <AdminMessage
            control={control}
            onConfirm={() => {
              setChangeMessage(false);
              onConfirm();
              setDone(true);
            }}
            onReject={() => {
              setChangeMessage(false);
              onReject();
            }}
          />
        ) : (
          <View style={styles.box}>
            <Text style={styles.question}>{done ? final : question}</Text>
            <TouchableOpacity
              style={styles.btnConfirm}
              onPress={() => {
                if (!done) {
                  if (withMessage) {
                    setChangeMessage(true);
                  } else {
                    onConfirm();
                    setDone(true);
                  }
                } else {
                  setDone(false);
                  onReject();
                }
              }}
            >
              <Text style={styles.confirmLabel}>
                {done ? "Kembali" : confirmLabel}
              </Text>
            </TouchableOpacity>
            {!done && (
              <TouchableOpacity onPress={onReject}>
                <Text style={styles.rejectLabel}>Batalkan</Text>
              </TouchableOpacity>
            )}
          </View>
        )}
      </BlurView>
    </Modal>
  );
};

export default AdminConfirmModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
  },
  loader: {
    backgroundColor: colors.White,
    padding: 20,
    borderRadius: 20,
  },
  box: {
    backgroundColor: colors.White,
    borderWidth: 1,
    borderColor: colors.Red,
    borderRadius: 15,
    paddingVertical: 14,
    paddingHorizontal: 18,
    alignItems: "center",
  },
  question: {
    fontFamily: fonts.Medium,
    fontSize: 18,
    color: colors.Black,
    marginBottom: 20,
  },
  btnConfirm: {
    backgroundColor: colors.Red,
    paddingHorizontal: 25,
    paddingVertical: 6,
    borderRadius: 50,
    marginBottom: 13,
  },
  confirmLabel: {
    fontFamily: fonts.Medium,
    fontSize: 13,
    color: colors.White,
  },
  rejectLabel: {
    fontFamily: fonts.Regular,
    fontSize: 13,
    color: colors.Placeholder,
  },
});
