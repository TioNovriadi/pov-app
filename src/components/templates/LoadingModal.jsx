import { View, Modal, StyleSheet, ActivityIndicator } from "react-native";
import React from "react";
import PropTypes from "prop-types";
import colors from "@themes/colors";

const LoadingModal = ({ visible }) => {
  return (
    <Modal transparent visible={visible}>
      <View style={styles.container}>
        <View style={styles.loader}>
          <ActivityIndicator size={"large"} />
        </View>
      </View>
    </Modal>
  );
};

LoadingModal.propTypes = {
  visible: PropTypes.bool,
};

export default LoadingModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.Modal,
    justifyContent: "center",
    alignItems: "center",
  },
  loader: {
    backgroundColor: colors.White,
    padding: 20,
    borderRadius: 15,
    justifyContent: "center",
    alignItems: "center",
  },
});
