import { Image, Modal, StyleSheet, Text, View } from "react-native";
import React from "react";
import { BlurView } from "expo-blur";
import colors from "@themes/colors";
import PropTypes from "prop-types";
import fonts from "@themes/fonts";
import POVButton from "@components/atoms/POVButton";
import POVTextButton from "@components/atoms/POVTextButton";

/**
 * Modal used for confirm after an event with confirm button and optional reject button
 *
 * @param {string} title - Title for the modal box if there's any
 * @param {object} titleIcon - Title icon that match with the title
 * @param {bool} visible - A boolean for show and hide the modal
 * @param {string} question - A confirmation question show in the box modal
 * @param {string} confirmLabel - Label for the confirmation button
 * @param {string} rejectLabel - Label for the rejection button
 * @param {func} onConfirm - Function that handle the confirmation when the confirm button pressed
 * @param {func} onReject - Function that handle the rejection when the reject button pressed
 * @returns {JSX.Element} - The entire modal screen
 */

const ConfirmModal = ({
  title,
  titleIcon,
  visible,
  question,
  confirmLabel,
  rejectLabel,
  onConfirm,
  onReject,
}) => {
  return (
    <Modal visible={visible} transparent>
      <BlurView style={styles.container} intensity={1} tint="light">
        <View style={styles.box}>
          {title && (
            <View style={styles.titleContainer}>
              <Image source={titleIcon} />
              <Text style={styles.title}>{title}</Text>
            </View>
          )}
          <Text style={styles.question}>{question}</Text>
          <POVButton label={confirmLabel} size="small" onPress={onConfirm} />
          {rejectLabel && (
            <POVTextButton label={rejectLabel} onPress={onReject} />
          )}
        </View>
      </BlurView>
    </Modal>
  );
};

ConfirmModal.propTypes = {
  title: PropTypes.string,
  titleIcon: PropTypes.node,
  visible: PropTypes.bool,
  question: PropTypes.string.isRequired,
  confirmLabel: PropTypes.string.isRequired,
  rejectLabel: PropTypes.string,
  onConfirm: PropTypes.func,
  onReject: PropTypes.func,
  extraStyle: PropTypes.object,
};

export default ConfirmModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
  },
  box: {
    backgroundColor: colors.White,
    borderWidth: 1,
    borderColor: colors.Red,
    borderRadius: 15,
    justifyContent: "center",
    alignItems: "center",
    gap: 14,
    paddingVertical: 24,
  },
  question: {
    fontFamily: fonts.Medium,
    fontSize: 17,
    color: colors.Black,
    textAlign: "center",
  },
  titleContainer: {
    flexDirection: "row",
    alignItems: "center",
    gap: 15,
  },
  title: {
    fontFamily: fonts.Medium,
    fontSize: 20,
    color: colors.Black,
  },
});
