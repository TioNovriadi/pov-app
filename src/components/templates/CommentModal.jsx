import { ActivityIndicator, Modal, StyleSheet, View } from "react-native";
import React from "react";
import { BlurView } from "expo-blur";
import AdminMessage from "@components/organisms/AdminMessage";
import colors from "@themes/colors";

const CommentModal = ({ visible, isLoading, control, onConfirm, onReject }) => {
  return (
    <Modal transparent visible={visible}>
      <BlurView
        style={[styles.container, { alignItems: isLoading ? "center" : null }]}
        tint="light"
        intensity={1}
      >
        {isLoading ? (
          <View style={styles.loader}>
            <ActivityIndicator size="large" />
          </View>
        ) : (
          <AdminMessage
            control={control}
            onConfirm={onConfirm}
            onReject={onReject}
          />
        )}
      </BlurView>
    </Modal>
  );
};

export default CommentModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
  },
  loader: {
    backgroundColor: colors.White,
    padding: 20,
    borderRadius: 20,
  },
});
