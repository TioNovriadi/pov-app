import { Modal, StyleSheet, Text, View } from "react-native";
import React from "react";
import { BlurView } from "expo-blur";

const MessageModal = ({ visible }) => {
  return (
    <Modal visible={visible} transparent>
      <BlurView style={styles.container} intensity={1} tint="light">
        <View style={styles.box}>
          <Text style={styles.question}>{question}</Text>
          <POVButton label={confirmLabel} size="small" onPress={onConfirm} />
          {rejectLabel && (
            <POVTextButton label={rejectLabel} onPress={onReject} />
          )}
        </View>
      </BlurView>
    </Modal>
  );
};

export default MessageModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
  },
});
