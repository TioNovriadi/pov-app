import { FlatList, StyleSheet, View } from "react-native";
import React from "react";
import ContentBox from "@components/molecules/ContentBox";

const ContentList = ({ data }) => {
  return (
    <View style={styles.container}>
      <FlatList
        data={data.contents}
        renderItem={({ item }) => (
          <ContentBox item={item} user={data.profile} />
        )}
      />
    </View>
  );
};

export default ContentList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
