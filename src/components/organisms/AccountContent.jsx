import { StyleSheet, Text, View } from "react-native";
import React from "react";
import AccountButtonList from "@components/molecules/AccountButtonList";

const AccountContent = () => {
  return (
    <View style={styles.container}>
      <AccountButtonList />
    </View>
  );
};

export default AccountContent;

const styles = StyleSheet.create({
  container: {
    marginTop: 50,
    marginLeft: 50,
    marginRight: 40,
  },
});
