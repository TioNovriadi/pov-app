import { View, StyleSheet, Image } from "react-native";
import React from "react";
import colors from "@themes/colors";
import POVBackButton from "@components/atoms/POVBackButton";
import POVHeaderButton from "@components/atoms/POVHeaderButton";
import POVLogoutButton from "@components/atoms/POVLogoutButton";

const Header = ({
  withBack = false,
  backDest,
  backFunc,
  withShadow = false,
  withBtn = false,
  btnLabel = "Lanjutkan",
  withLogout = false,
}) => {
  return (
    <View
      style={[styles.container, withShadow === true ? styles.shadow : null]}
    >
      {withBack === true && (
        <POVBackButton backDest={backDest} backFunc={backFunc} />
      )}

      {withLogout === true && <POVLogoutButton />}

      <View style={styles.logo}>
        <Image source={require("@assets/images/logo.png")} />
      </View>

      {withBtn === true && <POVHeaderButton btnLabel={btnLabel} />}
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: colors.White,
    height: 124,
    paddingHorizontal: 30,
  },
  logo: {
    position: "absolute",
    left: "50%",
    right: "50%",
    justifyContent: "center",
    alignItems: "center",
  },
  shadow: {
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.25,
    shadowColor: colors.Black,
    shadowRadius: 4,
    elevation: 5,
  },
});
