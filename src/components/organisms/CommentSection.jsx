import { StyleSheet, View } from "react-native";
import React from "react";
import CommentSectionHeader from "@components/molecules/CommentSectionHeader";
import CommentSectionList from "@components/molecules/CommentSectionList";

const CommentSection = ({ contentId, dataComment }) => {
  return (
    <View style={styles.container}>
      <CommentSectionHeader contentId={contentId} />
      <CommentSectionList data={dataComment} />
    </View>
  );
};

export default CommentSection;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 20,
    marginTop: 5,
    gap: 10,
  },
});
