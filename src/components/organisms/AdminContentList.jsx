import { FlatList, StyleSheet, View } from "react-native";
import React from "react";
import ContentBox from "@components/molecules/ContentBox";

const AdminContentList = ({ data }) => {
  return (
    <View>
      <FlatList
        contentContainerStyle={styles.list}
        data={data}
        renderItem={({ item }) => (
          <ContentBox item={item} user={item.author.profile} admin />
        )}
      />
    </View>
  );
};

export default AdminContentList;

const styles = StyleSheet.create({
  list: {
    paddingBottom: 200,
  },
});
