import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import React from "react";
import colors from "@themes/colors";
import fonts from "@themes/fonts";
import { useController } from "react-hook-form";

const AdminMessage = ({ control, onReject, onConfirm }) => {
  const { field } = useController({
    name: "message",
    defaultValue: null,
    control,
  });

  return (
    <View style={styles.container}>
      <View style={styles.block} />
      <View style={styles.formContainer}>
        <Text style={styles.title}>Tuliskan pesan anda</Text>
        <View style={styles.inputContainer}>
          <TextInput
            value={field.value}
            placeholder="Tuliskan alasan anda di sini"
            placeholderTextColor={colors.Placeholder}
            onChangeText={field.onChange}
            style={styles.input}
          />
        </View>
        <View style={styles.btnContainer}>
          <TouchableOpacity onPress={onReject}>
            <Text style={[styles.btnLabel, { color: colors.Red }]}>
              Batalkan
            </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={onConfirm}>
            <Text style={[styles.btnLabel, { color: colors.Black }]}>
              Kirim
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default AdminMessage;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.White,
    borderWidth: 1,
    borderColor: colors.Red,
    borderRadius: 15,
    flexDirection: "row",
    height: 153,
  },
  block: {
    width: 30,
    backgroundColor: colors.Red,
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
  },
  formContainer: {
    flex: 1,
    paddingLeft: 13,
    paddingRight: 43,
    paddingVertical: 20,
    gap: 10,
  },
  title: {
    fontFamily: fonts.Medium,
    fontSize: 18,
    color: colors.Black,
  },
  inputContainer: {
    height: 54,
    borderWidth: 0.4,
    borderColor: colors.Black,
    borderRadius: 6,
    paddingHorizontal: 10,
    paddingVertical: 8,
  },
  input: {
    fontFamily: fonts.Regular,
    fontSize: 10,
    color: colors.Black,
  },
  btnContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    gap: 23,
  },
  btnLabel: {
    fontFamily: fonts.Regular,
    fontSize: 13,
  },
});
