import { StyleSheet, Text, View } from "react-native";
import React from "react";
import PropTypes from "prop-types";
import fonts from "@themes/fonts";
import colors from "@themes/colors";
import POVButton from "@components/atoms/POVButton";
import FormInput from "@components/molecules/FormInput";
import { useForm } from "react-hook-form";

/**
 * Renders a customizable form component with input fields and a submit button.
 *
 * @param {boolean} withTitle - Determines whether to display a title above the form.
 * @param {string} title - The title to be displayed above the form.
 * @param {object} extraStyle - Additional styles to be applied to the form container.
 * @param {boolean} withSubmit - Determines whether to display a submit button below the form.
 * @param {string} submitLabel - The label to be displayed on the submit button.
 * @param {function} onSubmit - The function to be called when the submit button is pressed.
 * @param {array} dataInput - An array of objects that define the input fields of the form.
 * @param {object} controlPass - An optional object that can be used to pass a pre-existing control object to the FormInput component.
 *
 * @returns {JSX.Element} A View component that contains the form, with optional title and submit button.
 */

const Form = ({
  withTitle,
  title,
  extraStyle,
  withSubmit,
  submitLabel,
  onSubmit,
  dataInput,
  controlPass,
  app,
}) => {
  const { control, handleSubmit } = useForm();

  return (
    <View style={[styles.container, extraStyle]}>
      {withTitle === true && <Text style={styles.title}>{title}</Text>}
      <FormInput
        dataInput={dataInput}
        control={controlPass ? controlPass : control}
        app={app}
      />
      {withSubmit === true && (
        <View style={styles.btnContainer}>
          <POVButton
            label={submitLabel}
            size="large"
            onPress={handleSubmit(onSubmit)}
          />
        </View>
      )}
    </View>
  );
};

Form.defaultProps = {
  submitLabel: "Masuk",
  withSubmit: true,
};

Form.propTypes = {
  withTitle: PropTypes.bool,
  title: PropTypes.string,
  extraStyle: PropTypes.object,
  onSubmit: PropTypes.func,
  submitLabel: PropTypes.string,
  dataInput: PropTypes.arrayOf(PropTypes.object),
  withSubmit: PropTypes.bool,
  app: PropTypes.bool,
};

export default Form;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 38,
  },
  title: {
    fontFamily: fonts.Medium,
    fontSize: 20,
    color: colors.Black,
    marginBottom: 54,
  },
  btnContainer: {
    alignItems: "center",
    marginTop: 37,
  },
});
