import { FlatList, StyleSheet, View } from "react-native";
import React from "react";
import ContentBox from "@components/molecules/ContentBox";

const HomeContentList = ({ data, bookmark }) => {
  return (
    <View style={styles.container}>
      <FlatList
        data={data}
        renderItem={({ item }) => {
          if (item.posted) {
            return (
              <ContentBox
                item={item.content}
                user={item.content.author.profile}
                home
                editor={item.editor.profile.username}
                bookmark={bookmark ? "bookmarkPage" : item.bookmarks}
              />
            );
          }
        }}
      />
    </View>
  );
};

export default HomeContentList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
