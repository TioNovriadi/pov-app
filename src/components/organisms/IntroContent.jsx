import { StyleSheet, Text, View } from "react-native";
import React from "react";
import fonts from "@themes/fonts";
import colors from "@themes/colors";
import IntroNavigation from "@components/molecules/IntroNavigation";

const IntroContent = ({ desc, withConfirm }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.desc}>{desc}</Text>
      <IntroNavigation withConfirm={withConfirm} />
    </View>
  );
};

export default IntroContent;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 24,
  },
  desc: {
    fontFamily: fonts.Medium,
    fontSize: 20,
    color: colors.Black,
    marginTop: 143,
  },
});
