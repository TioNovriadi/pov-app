import { StyleSheet, Text, TouchableOpacity } from "react-native";
import React from "react";
import fonts from "@themes/fonts";
import colors from "@themes/colors";
import PropTypes from "prop-types";

/**
 * Renders a header button with a label.
 *
 * @param {string} btnLabel - The label of the button.
 * @returns {TouchableOpacity} The custom button component.
 */

const POVHeaderButton = ({ btnLabel }) => {
  return (
    <TouchableOpacity>
      <Text style={styles.label}>{btnLabel}</Text>
    </TouchableOpacity>
  );
};

POVHeaderButton.propTypes = {
  btnLabel: PropTypes.string.isRequired,
};

export default POVHeaderButton;

const styles = StyleSheet.create({
  label: {
    fontFamily: fonts.Medium,
    fontSize: 15,
    color: colors.Black,
    textShadowOffset: {
      width: 0,
      height: 4,
    },
    textShadowColor: colors.BlurBlack,
    textShadowRadius: 4,
  },
});
