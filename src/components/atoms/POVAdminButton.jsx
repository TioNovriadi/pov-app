import { Image, StyleSheet, Text, TouchableOpacity } from "react-native";
import React from "react";
import colors from "@themes/colors";
import fonts from "@themes/fonts";

const POVAdminButton = ({
  icon,
  label,
  onPress,
  disabled = false,
  background = colors.DarkGrey,
}) => {
  return (
    <TouchableOpacity
      style={[styles.container, { backgroundColor: background }]}
      onPress={onPress}
      disabled={disabled}
    >
      <Image source={icon} />
      <Text style={styles.label}>{label}</Text>
    </TouchableOpacity>
  );
};

export default POVAdminButton;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    gap: 6,
    borderRadius: 5,
    flex: 1,
    justifyContent: "center",
    paddingVertical: 7,
  },
  label: {
    fontFamily: fonts.Medium,
    fontSize: 10,
    color: colors.White,
  },
});
