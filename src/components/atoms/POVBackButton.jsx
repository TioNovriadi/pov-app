import { Image, TouchableOpacity } from "react-native";
import React from "react";
import { useRecoilValue } from "recoil";
import { navState } from "@store/navState";

/**
 * Renders a TouchableOpacity with an Image inside. When the TouchableOpacity is pressed,
 * it executes a back function (if provided) and navigates to a specified destination using
 * the 'nav' object from the 'react-navigation' library.
 *
 * @param {Function} backFunc - Optional function to be executed when the TouchableOpacity is pressed before navigating to the back destination.
 * @param {string} backDest - The destination to navigate to when the TouchableOpacity is pressed.
 * @returns {TouchableOpacity} - A TouchableOpacity component with an Image inside.
 */

const POVBackButton = ({ backDest, backFunc }) => {
  const nav = useRecoilValue(navState);

  return (
    <TouchableOpacity
      onPress={() => {
        if (backFunc) {
          backFunc();
        }
        nav.navigate(backDest);
      }}
    >
      <Image source={require("@assets/images/back.png")} />
    </TouchableOpacity>
  );
};

export default POVBackButton;
