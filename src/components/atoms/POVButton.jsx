import { Text, TouchableOpacity, StyleSheet } from "react-native";
import React from "react";
import colors from "@themes/colors";
import PropTypes from "prop-types";
import fonts from "@themes/fonts";

/**
 * Creates a customizable button component for a React Native application.
 *
 * @param {string} label - The text displayed on the button.
 * @param {function} onPress - The function called when the button is pressed.
 * @param {string} size - The size of the button (large, medium, or small).
 * @param {string} color - The background color of the button.
 * @returns {TouchableOpacity} - The button component.
 */

const POVButton = ({ label, size, onPress, color, withShadow }) => {
  return (
    <TouchableOpacity
      style={[
        styles.container,
        withShadow === true ? styles.shadow : null,
        {
          width: size === "large" ? 258 : size === "medium" ? 153 : 116,
          height: size === "large" ? 46 : size === "medium" ? 32 : 30,
        },
        { backgroundColor: color },
      ]}
      onPress={onPress}
    >
      <Text
        style={[
          styles.label,
          { fontSize: size === "large" ? 18 : size === "medium" ? 16 : 13 },
        ]}
      >
        {label}
      </Text>
    </TouchableOpacity>
  );
};

POVButton.defaultProps = {
  size: "large",
  color: colors.Red,
  withShadow: false,
};

POVButton.propTypes = {
  label: PropTypes.string.isRequired,
  size: PropTypes.oneOf(["small", "medium", "large"]),
  onPress: PropTypes.func.isRequired,
  color: PropTypes.string,
  withShadow: PropTypes.bool,
};

export default POVButton;

const styles = StyleSheet.create({
  container: {
    borderRadius: 45,
    justifyContent: "center",
    alignItems: "center",
  },
  label: {
    fontFamily: fonts.Medium,
    color: colors.White,
  },
  shadow: {
    shadowColor: colors.Black,
    shadowOpacity: 0.25,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 4,
    elevation: 5,
  },
});
