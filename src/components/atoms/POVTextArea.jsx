import { StyleSheet, Text, TextInput, View } from "react-native";
import React, { useEffect, useState } from "react";
import colors from "@themes/colors";
import fonts from "@themes/fonts";
import { useController } from "react-hook-form";
import PropTypes from "prop-types";

/**
 * Renders a text input field with an optional title and error message.
 * This component is designed to be used in forms and integrates with the 'react-hook-form' library for form validation.
 *
 * @param {string} title - The title of the input field.
 * @param {string} name - The name of the input field.
 * @param {string} defaultValue - The default value of the input field.
 * @param {object} control - The control object used by the 'react-hook-form' library to control the input field.
 * @param {string} placeholder - The placeholder text of the input field.
 * @param {boolean} required - Indicates whether the input field is required or not.
 * @param {array} errorCheck - An array of objects that contains error messages for the form fields.
 * @returns {JSX.Element} - The rendered text input field.
 */
const POVTextArea = ({
  title,
  name,
  defaultValue,
  control,
  placeholder,
  required,
  errorCheck,
}) => {
  const { field } = useController({ name, defaultValue, control });

  const [err, setErr] = useState(null);

  useEffect(() => {
    setErr(null);
    if (errorCheck) {
      const errorFilter = errorCheck.filter((tmp) => tmp.field === name);

      if (errorFilter.length > 0) {
        setErr(errorFilter[0]);
      }
    }
  }, [errorCheck]);

  return (
    <>
      {title && (
        <Text style={styles.title}>
          {title}
          {required === true && <Text style={styles.star}>*</Text>}
        </Text>
      )}
      <View style={styles.container}>
        <TextInput
          value={field.value}
          placeholder={placeholder}
          placeholderTextColor={colors.Placeholder}
          onChangeText={field.onChange}
          multiline
          style={styles.input}
        />
      </View>
      {err !== null && <Text style={styles.error}>{err.message}</Text>}
    </>
  );
};

POVTextArea.propTypes = {
  title: PropTypes.string,
  name: PropTypes.string.isRequired,
  defaultValue: PropTypes.string,
  control: PropTypes.object.isRequired,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  errorCheck: PropTypes.array,
};

export default POVTextArea;

const styles = StyleSheet.create({
  container: {
    height: 300,
    borderWidth: 1,
    borderColor: colors.Black,
    borderRadius: 10,
    paddingHorizontal: 32,
    paddingVertical: 10,
  },
  title: {
    fontFamily: fonts.Regular,
    fontSize: 18,
    color: colors.Black,
    marginLeft: 32,
    marginBottom: 5,
  },
  input: {
    fontFamily: fonts.Regular,
    fontSize: 13,
    color: colors.Black,
  },
  error: {
    fontFamily: fonts.Regular,
    fontSize: 10,
    color: colors.Red,
    marginLeft: 32,
  },
});
