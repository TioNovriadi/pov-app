import { StyleSheet, Text, TextInput, View } from "react-native";
import React, { useEffect, useState } from "react";
import colors from "@themes/colors";
import { useController } from "react-hook-form";
import fonts from "@themes/fonts";
import PropTypes from "prop-types";

/**
 * Renders a rounded text input with a title and optional error message.
 *
 * @param {Object} props - The input props.
 * @param {string} props.title - The title of the input field.
 * @param {string} props.name - The name of the input field.
 * @param {string} props.defaultValue - The default value of the input field.
 * @param {Object} props.control - The control methods for the input field.
 * @param {string} props.placeholder - The placeholder text of the input field.
 * @param {boolean} props.isPassword - Determines whether the input field is a password field or not.
 * @param {boolean} props.required - Determines whether the input field is required or not.
 * @param {Array} props.errorCheck - The error messages for the input fields.
 * @returns {JSX.Element} - The rendered component.
 */
const POVRoundTextInput = ({
  title,
  name,
  defaultValue,
  control,
  placeholder,
  isPassword,
  required,
  errorCheck,
}) => {
  const { field } = useController({
    name,
    defaultValue,
    control,
  });

  const [err, setErr] = useState(null);

  useEffect(() => {
    setErr(null);
    if (errorCheck) {
      const errorFilter = errorCheck.filter((tmp) => tmp.field === name);

      if (errorFilter.length > 0) {
        setErr(errorFilter[0]);
      }
    }
  }, [errorCheck]);

  return (
    <>
      {title && (
        <Text style={styles.title}>
          {title}
          {required === true && <Text style={styles.star}>*</Text>}
        </Text>
      )}
      <View style={styles.container}>
        <TextInput
          value={field.value}
          placeholder={placeholder}
          placeholderTextColor={colors.Placeholder}
          onChangeText={field.onChange}
          style={styles.input}
          secureTextEntry={isPassword}
        />
      </View>
      {err !== null && <Text style={styles.error}>{err.message}</Text>}
    </>
  );
};

POVRoundTextInput.defaultProps = {
  isPassword: false,
};

POVRoundTextInput.propTypes = {
  title: PropTypes.string,
  name: PropTypes.string.isRequired,
  defaultValue: PropTypes.string,
  placeholder: PropTypes.string.isRequired,
  isPassword: PropTypes.bool,
  required: PropTypes.bool,
};

export default POVRoundTextInput;

const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    borderColor: colors.Black,
    borderRadius: 25,
    justifyContent: "center",
    paddingHorizontal: 32,
    paddingVertical: 10,
  },
  input: {
    fontFamily: fonts.Regular,
    fontSize: 13,
    color: colors.Black,
  },
  title: {
    fontFamily: fonts.Regular,
    fontSize: 18,
    color: colors.Black,
    marginLeft: 32,
    marginBottom: 5,
  },
  star: {
    color: colors.Red,
  },
  error: {
    fontFamily: fonts.Regular,
    fontSize: 10,
    color: colors.Red,
    marginLeft: 32,
  },
});
