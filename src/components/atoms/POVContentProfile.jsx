import { Image, StyleSheet } from "react-native";
import React from "react";

const POVContentProfile = ({ image }) => {
  return <Image style={styles.image} source={image} />;
};

export default POVContentProfile;

const styles = StyleSheet.create({
  image: {
    width: 41.55,
    height: 40.13,
    borderRadius: 50,
  },
});
