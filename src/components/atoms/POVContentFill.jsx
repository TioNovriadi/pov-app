import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import React, { useState } from "react";
import fonts from "@themes/fonts";
import colors from "@themes/colors";
import { API_ACCESS } from "@utils/config/endpoint";
import POVAdminButton from "./POVAdminButton";
import { useRecoilState, useRecoilValue } from "recoil";
import { refreshState } from "@store/refreshState";
import AdminConfirmModal from "@components/templates/AdminConfirmModal";
import useFetchData from "@hooks/useFetchData";
import { useForm } from "react-hook-form";
import { userIdState } from "@store/authState";
import { navState } from "@store/navState";

const POVContentFill = ({
  item,
  username,
  editorUsername,
  admin,
  home,
  bookmark,
}) => {
  const [refresh, setRefresh] = useRecoilState(refreshState);
  const userId = useRecoilValue(userIdState);
  const nav = useRecoilValue(navState);

  const [modalShowPublish, setModalShowPublish] = useState(false);
  const [modalShowReject, setModalShowReject] = useState(false);
  const [modalShowDelete, setModalShowDelete] = useState(false);

  const { fetchFunction, isLoading } = useFetchData();
  const { control, handleSubmit } = useForm();

  const handleReject = (data) => {
    const body = new FormData();
    body.append("message", data.message);

    fetchFunction("POST", API_ACCESS.rejectContentAdmin + `/${item.id}`, body);
    setRefresh(!refresh);
  };

  const handlePublish = () => {
    fetchFunction("POST", API_ACCESS.acceptContentAdmin + `/${item.id}`);
    setRefresh(!refresh);
  };

  const handleDelete = () => {
    fetchFunction("DELETE", API_ACCESS.deleteContentAdmin + `/${item.id}`);
    setRefresh(!refresh);
  };

  const handleLike = () => {
    const checkLike = item.likes.filter((tmp) => tmp.id === userId);

    if (checkLike.length > 0) {
      fetchFunction("DELETE", API_ACCESS.unlike + `/${item.id}`);
    } else {
      fetchFunction("POST", API_ACCESS.like + `/${item.id}`);
    }

    setRefresh(!refresh);
  };

  const handleBookmark = () => {
    if (bookmark === "bookmarkPage") {
      fetchFunction("DELETE", API_ACCESS.unbookmark + `/${item.id}`);
    } else {
      const checkBookmark = bookmark.filter((tmp) => tmp.id === userId);

      if (checkBookmark.length > 0) {
        fetchFunction("DELETE", API_ACCESS.unbookmark + `/${item.id}`);
      } else {
        fetchFunction("POST", API_ACCESS.bookmark + `/${item.id}`);
      }
    }

    setRefresh(!refresh);
  };

  const handleComment = () => {
    nav.navigate("Comment", {
      data: item,
      editorUsername,
      bookmark,
    });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.username}>{item.anonymous ? "Anon" : username}</Text>
      <Image
        source={{
          uri: `${API_ACCESS.contentAsset}/${item.image}`,
        }}
        style={styles.image}
      />
      <Text style={styles.title}>{item.title}</Text>
      <Text style={styles.story}>{item.story}</Text>
      {admin ? (
        <View style={styles.adminButtonContainer}>
          <POVAdminButton
            icon={
              item.checked
                ? item.checkedContent.posted
                  ? require("@assets/images/check.png")
                  : require("@assets/images/x.png")
                : require("@assets/images/check.png")
            }
            label={
              item.checked
                ? item.checkedContent.posted
                  ? "Diterbitkan"
                  : "Ditolak"
                : "Terbitkan"
            }
            background={
              item.checked
                ? item.checkedContent.posted
                  ? colors.Success
                  : colors.Red
                : colors.DarkGrey
            }
            disabled={item.checked ? true : false}
            onPress={() => setModalShowPublish(true)}
          />
          <POVAdminButton
            icon={require("@assets/images/edit.png")}
            label="Sunting"
            background={item.checked ? colors.Off : colors.DarkGrey}
            onPress={() =>
              nav.navigate("EditContentAdmin", {
                data: item,
              })
            }
            disabled={item.checked ? true : false}
          />
          <POVAdminButton
            icon={
              item.checked
                ? require("@assets/images/trash.png")
                : require("@assets/images/x.png")
            }
            label={item.checked ? "Hapus" : "Tolak"}
            background={item.checked ? colors.Red : colors.DarkGrey}
            onPress={() => {
              if (!item.checked) {
                setModalShowReject(true);
              } else {
                setModalShowDelete(true);
              }
            }}
          />
        </View>
      ) : home ? (
        <View>
          <Text style={styles.label}>
            Penulis <Text style={styles.name}>{username}</Text>
          </Text>
          <Text style={styles.label}>
            Verifikasi oleh <Text style={styles.name}>{editorUsername}</Text>
          </Text>
          <View style={styles.actionContainer}>
            <View style={styles.contentActionContainer}>
              <TouchableOpacity style={styles.btn} onPress={handleComment}>
                <Image source={require("@assets/images/comment.png")} />
                <Text style={styles.btnLabel}>{item.comments.length}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.btn} onPress={handleLike}>
                <Image
                  source={
                    item.likes.filter((tmp) => tmp.id === userId).length > 0
                      ? require("@assets/images/likeActive.png")
                      : require("@assets/images/like.png")
                  }
                />
                <Text style={styles.btnLabel}>{item.likes.length}</Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity onPress={handleBookmark}>
              <Image
                source={
                  bookmark === "bookmarkPage"
                    ? require("@assets/images/bookmarkActive.png")
                    : bookmark.filter((tmp) => tmp.id === userId).length > 0
                    ? require("@assets/images/bookmarkActive.png")
                    : require("@assets/images/bookmark.png")
                }
              />
            </TouchableOpacity>
          </View>
        </View>
      ) : (
        <View
          style={[
            styles.statusMessageContainer,
            {
              alignSelf: item.checked
                ? item.checkedContent.message
                  ? null
                  : "flex-start"
                : "flex-start",
              borderColor: !item.checked
                ? colors.DarkGrey
                : item.checkedContent.posted
                ? colors.Success
                : colors.Red,
            },
          ]}
        >
          <View
            style={[
              styles.statusContainer,
              {
                backgroundColor: !item.checked
                  ? colors.DarkGrey
                  : item.checkedContent.posted
                  ? colors.Success
                  : colors.Red,
                alignSelf: item.checked
                  ? item.checkedContent.message
                    ? null
                    : "flex-start"
                  : "flex-start",
              },
            ]}
          >
            <Image
              source={
                !item.checked
                  ? require("@assets/images/tertunda.png")
                  : item.checkedContent.posted
                  ? require("@assets/images/diterbitkan.png")
                  : require("@assets/images/ditolak.png")
              }
            />
            <Text style={styles.status}>
              {!item.checked
                ? "Tertunda"
                : item.checkedContent.posted
                ? "Diterbitkan"
                : "Ditolak"}
            </Text>
          </View>
          {item.checked ? (
            item.checkedContent.message ? (
              <Text style={styles.message}>{item.checkedContent.message}</Text>
            ) : null
          ) : null}
        </View>
      )}

      <AdminConfirmModal
        visibile={modalShowReject}
        question="Apakah anda ingin menolak berita ini?"
        confirmLabel="Tolak"
        onReject={() => setModalShowReject(false)}
        withMessage
        onConfirm={handleSubmit(handleReject)}
        control={control}
        isLoading={isLoading}
        final="Terima kasih! Berita ini sudah ditolak"
      />

      <AdminConfirmModal
        visibile={modalShowPublish}
        question="Apakah anda ingin menerbitkan berita ini?"
        confirmLabel="Terbitkan"
        onReject={() => setModalShowPublish(false)}
        onConfirm={handlePublish}
        isLoading={isLoading}
        final="Terima kasih! Berita ini sudah diterbitkan"
      />

      <AdminConfirmModal
        visibile={modalShowDelete}
        question="Apakah anda ingin menghapus berita ini?"
        confirmLabel="Hapus"
        onReject={() => setModalShowDelete(false)}
        onConfirm={handleDelete}
        isLoading={isLoading}
        final="Terima kasih! Berita ini sudah dihapus"
      />
    </View>
  );
};

export default POVContentFill;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  username: {
    fontFamily: fonts.Regular,
    fontSize: 14,
    color: colors.Black,
    marginBottom: 9,
  },
  image: {
    borderWidth: 1,
    height: 120,
    resizeMode: "cover",
    marginBottom: 15,
  },
  title: {
    fontFamily: fonts.Bold,
    fontSize: 13,
    color: colors.Black,
    marginBottom: 4,
  },
  story: {
    fontFamily: fonts.Regular,
    fontSize: 11,
    color: colors.Black,
    marginBottom: 15,
  },
  statusContainer: {
    flexDirection: "row",
    alignItems: "center",
    gap: 4,
    paddingHorizontal: 5,
    paddingVertical: 3,
    borderRadius: 5,
  },
  statusMessageContainer: {
    borderWidth: 0.3,
    borderRadius: 5,
  },
  status: {
    fontFamily: fonts.Medium,
    fontSize: 10,
    color: colors.White,
  },
  adminButtonContainer: {
    flexDirection: "row",
    alignItems: "center",
    gap: 20,
  },
  message: {
    marginHorizontal: 28,
    marginVertical: 16,
    fontFamily: fonts.Regular,
    fontSize: 10,
    color: colors.Black,
  },
  label: {
    fontFamily: fonts.Medium,
    fontSize: 11,
    color: colors.Black,
    marginBottom: 6,
  },
  name: {
    fontFamily: fonts.Regular,
    color: colors.Red,
  },
  actionContainer: {
    marginTop: 11,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  contentActionContainer: {
    flexDirection: "row",
    alignItems: "center",
    gap: 40,
  },
  btn: {
    flexDirection: "row",
    alignItems: "center",
    gap: 12,
  },
  btnLabel: {
    fontFamily: fonts.Medium,
    fontSize: 8,
    color: colors.Placeholder,
  },
});
