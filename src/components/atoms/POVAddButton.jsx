import { StyleSheet, Text, TouchableOpacity } from "react-native";
import React from "react";
import colors from "@themes/colors";
import fonts from "@themes/fonts";
import { useRecoilValue } from "recoil";
import { navState } from "@store/navState";

/**
 * Renders a button with a plus sign that navigates to the 'AddContent' screen when pressed.
 *
 * @returns {React.Component} The rendered button component.
 */

const POVAddButton = () => {
  const nav = useRecoilValue(navState);

  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => nav.navigate("AddContent")}
    >
      <Text style={styles.plus}>+</Text>
    </TouchableOpacity>
  );
};

export default POVAddButton;

const styles = StyleSheet.create({
  container: {
    width: 67,
    height: 67,
    backgroundColor: colors.Red,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    position: "absolute",
    bottom: 0,
    right: 20,
    bottom: 20,
  },
  plus: {
    fontFamily: fonts.Regular,
    fontSize: 40,
    color: colors.White,
    includeFontPadding: false,
  },
});
