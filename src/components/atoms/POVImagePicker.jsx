import { Dimensions, Image, StyleSheet, TouchableOpacity } from "react-native";
import React from "react";
import colors from "@themes/colors";
import { useController } from "react-hook-form";
import PropTypes from "prop-types";
import * as ImagePicker from "expo-image-picker";
import * as mime from "react-native-mime-types";
import { API_ACCESS } from "@utils/config/endpoint";

const WIDTH = Dimensions.get("window").width;

/**
 * A React component that allows the user to pick an image from their device's image library and display it in a container.
 *
 * @param {Object} props - The component props.
 * @param {string} props.name - The name of the input field.
 * @param {string} props.defaultValue - The default value of the input field.
 * @param {Object} props.control - An object containing the methods to control the input field.
 * @param {Object} props.placeholder - An image to display when no image has been selected.
 * @returns {TouchableOpacity} - A TouchableOpacity component that displays the selected image or a placeholder image.
 */
const POVImagePicker = ({
  name,
  defaultValue,
  control,
  placeholder,
  disable,
}) => {
  const { field } = useController({
    name,
    defaultValue,
    control,
  });

  /**
   * Launches the image library and allows the user to select an image.
   * Updates the input field's value with the selected image's URI, type, and name.
   */
  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [16, 9],
      quality: 1,
    });

    if (!result.canceled) {
      let filename = result.assets[0].uri.substring(
        result.assets[0].uri.lastIndexOf("/") + 1,
        result.assets[0].uri.length
      );

      field.onChange({
        uri: result.assets[0].uri,
        type: mime.lookup(filename),
        name: filename,
      });
    }
  };

  return (
    <TouchableOpacity
      style={styles.container}
      onPress={pickImage}
      disabled={disable}
    >
      <Image
        source={
          field.value === null
            ? placeholder
            : {
                uri: disable
                  ? API_ACCESS.contentAsset + `/${field.value}`
                  : field.value.uri,
              }
        }
        style={field.value !== null ? styles.image : null}
      />
    </TouchableOpacity>
  );
};

POVImagePicker.propTypes = {
  name: PropTypes.string.isRequired,
  defaultValue: PropTypes.string,
  placeholder: PropTypes.node.isRequired,
  control: PropTypes.object.isRequired,
};

export default POVImagePicker;

const styles = StyleSheet.create({
  container: {
    height: 143,
    borderWidth: 1,
    borderColor: colors.Black,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    width: WIDTH - 78,
    height: 141,
    resizeMode: "cover",
    borderRadius: 9,
  },
});
