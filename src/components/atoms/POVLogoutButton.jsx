import { Image, StyleSheet, TouchableOpacity } from "react-native";
import React from "react";
import useAuth from "@hooks/useAuth";

const POVLogoutButton = () => {
  const { logout } = useAuth();

  return (
    <TouchableOpacity onPress={logout} style={styles.container}>
      <Image source={require("@assets/images/logoutSmall.png")} />
    </TouchableOpacity>
  );
};

export default POVLogoutButton;

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
  },
});
