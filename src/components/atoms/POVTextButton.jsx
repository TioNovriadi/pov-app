import { Text, TouchableOpacity, StyleSheet } from "react-native";
import React from "react";
import fonts from "@themes/fonts";
import colors from "@themes/colors";
import PropTypes from "prop-types";

/**
 * Renders a rounded text input component with customizable styles and properties.
 *
 * @param {string} label - The label text to be displayed on the button (required).
 * @param {function} onPress - The action to be taken when the button is pressed (required).
 * @param {string} color - The color of the button (default is 'DarkGrey').
 * @param {object} extraStyle - Additional styles to be applied to the button.
 * @param {boolean} withShadow - Whether or not to apply a text shadow to the button (default is false).
 *
 * @returns {JSX.Element} - A rounded text input component.
 */
const POVTextButton = ({ label, onPress, color, extraStyle, withShadow }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Text
        style={[
          styles.label,
          extraStyle,
          { color },
          withShadow === true ? styles.shadow : null,
        ]}
      >
        {label}
      </Text>
    </TouchableOpacity>
  );
};

POVTextButton.defaultProps = {
  color: colors.DarkGrey,
  withShadow: false,
};

POVTextButton.propTypes = {
  label: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  color: PropTypes.string,
  extraStyle: PropTypes.object,
  withShadow: PropTypes.bool,
};

export default POVTextButton;

const styles = StyleSheet.create({
  label: {
    fontFamily: fonts.Regular,
    fontSize: 10,
    textAlign: "center",
  },
  shadow: {
    textShadowColor: colors.BlurBlack,
    textShadowOffset: {
      width: 0,
      height: 4,
    },
    textShadowRadius: 4,
  },
});
