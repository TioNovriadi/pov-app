import { Image, StyleSheet, Text, View } from "react-native";
import React from "react";
import colors from "@themes/colors";
import fonts from "@themes/fonts";

const CommentBox = ({ item }) => {
  return (
    <View style={styles.container}>
      <View style={styles.block} />
      <View style={styles.descContainer}>
        <View style={styles.accountContainer}>
          <Image
            source={
              item.user.profile.profile_pic
                ? { uri: item.user.profile.profile_pic }
                : require("@assets/images/userSmall.png")
            }
          />
          <Text style={styles.username}>{item.user.profile.username}</Text>
        </View>
        <Text style={styles.comment}>{item.comment}</Text>
      </View>
    </View>
  );
};

export default CommentBox;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    minHeight: 82,
    backgroundColor: colors.LightGrey,
    borderRadius: 10,
  },
  block: {
    width: 15,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    backgroundColor: colors.Red,
  },
  descContainer: {
    flex: 1,
    paddingLeft: 9,
    paddingRight: 25,
    paddingVertical: 10,
    gap: 6,
  },
  accountContainer: {
    flexDirection: "row",
    alignItems: "center",
    gap: 11.27,
  },
  username: {
    fontFamily: fonts.Medium,
    fontSize: 12,
    color: colors.Black,
  },
  comment: {
    fontFamily: fonts.Regular,
    fontSize: 10,
    color: colors.Black,
  },
});
