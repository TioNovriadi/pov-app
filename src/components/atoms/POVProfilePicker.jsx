import { Image, StyleSheet, TouchableOpacity, View } from "react-native";
import React from "react";
import { useController } from "react-hook-form";
import { API_ACCESS } from "@utils/config/endpoint";
import * as ImagePicker from "expo-image-picker";
import * as mime from "react-native-mime-types";

const POVProfilePicker = ({ name, defaultValue, control }) => {
  const { field } = useController({
    name,
    defaultValue,
    control,
  });

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    let filename = result.assets[0].uri.substring(
      result.assets[0].uri.lastIndexOf("/") + 1,
      result.assets[0].uri.length
    );

    if (!result.canceled) {
      field.onChange({
        uri: result.assets[0].uri,
        type: mime.lookup(filename),
        name: filename,
      });
    }
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={pickImage}>
        <Image
          source={
            field.value
              ? {
                  uri:
                    defaultValue === field.value
                      ? "http://10.0.2.2:3333/" +
                        API_ACCESS.userAsset +
                        `/${field.value}`
                      : field.value.uri,
                }
              : require("@assets/images/profilePic.png")
          }
          style={field.value && styles.image}
        />
      </TouchableOpacity>
    </View>
  );
};

export default POVProfilePicker;

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
  },
  image: {
    width: 96,
    height: 96,
    borderRadius: 50,
    resizeMode: "cover",
  },
});
