import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import React from "react";
import PropTypes from "prop-types";
import fonts from "@themes/fonts";
import colors from "@themes/colors";

const POVAccountButton = ({ icon, label, onPress }) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <View style={styles.labelContainer}>
        <Image source={icon} />
        <Text style={styles.label}>{label}</Text>
      </View>
      <Image source={require("@assets/images/arrow.png")} />
    </TouchableOpacity>
  );
};

POVAccountButton.propTypes = {
  icon: PropTypes.node,
  label: PropTypes.string,
};

export default POVAccountButton;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  labelContainer: {
    flexDirection: "row",
    alignItems: "center",
    gap: 30,
  },
  label: {
    fontFamily: fonts.Medium,
    fontSize: 22,
    color: colors.Black,
  },
});
