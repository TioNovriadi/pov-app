import React from "react";
import { RecoilRoot } from "recoil";
import AppNav from "@navigation/AppNav";
import { useFonts } from "expo-font";
import * as SplashScreen from "expo-splash-screen";
import { ToastProvider } from "react-native-toast-notifications";
import { QueryClient, QueryClientProvider } from "react-query";

SplashScreen.preventAutoHideAsync();

const App = () => {
  const queryClient = new QueryClient();

  const [fontsLoaded] = useFonts({
    "Made-Tommy-Regular": require("@assets/fonts/MADE_TOMMY_Regular.otf"),
    "Made-Tommy-Medium": require("@assets/fonts/MADE_TOMMY_Medium.otf"),
    "Made-Tommy-Bold": require("@assets/fonts/MADE_TOMMY_Bold.otf"),
  });

  return (
    <QueryClientProvider client={queryClient}>
      <ToastProvider duration={1500}>
        <RecoilRoot>
          <AppNav fontsLoaded={fontsLoaded} />
        </RecoilRoot>
      </ToastProvider>
    </QueryClientProvider>
  );
};

export default App;
